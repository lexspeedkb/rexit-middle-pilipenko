<?php

use App\Address;
use App\EchoSender;
use App\Logger;
use App\Message;
use App\Notifier;

require __DIR__ . '/../vendor/autoload.php';

if ($argc == 4) {
    $receiveData['to']      = $argv[1];
    $receiveData['subject'] = $argv[2];
    $receiveData['message'] = $argv[3];
}

$echoSender = new EchoSender();
$logger = new Logger();
$notifier = new Notifier($echoSender, $logger);
$notifier->send(
    new Message($receiveData['subject'], $receiveData['message']),
    new Address($receiveData['to'])
);
