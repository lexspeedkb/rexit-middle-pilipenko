<?php
declare(strict_types=1);


namespace App;


final class Notifier
{
    private LoggerInterface $logger;
    private SenderInterface $sender;

    public function __construct(SenderInterface $sender, LoggerInterface $logger)
    {
        $this->sender = $sender;
        $this->logger = $logger;
    }

    public function send(Message $message, Address $to)
    {
        if (!filter_var($to->receiver(), FILTER_VALIDATE_EMAIL)) {
            echo "ERROR: Email address invalid.\n";
            return 0;
        }

        if (strlen($message->message()) > 1024) {
            echo "ERROR: Message is longer than 1024 symbols.\n";
            return 0;
        }

        if (strlen($message->subject()) > 32) {
            echo "ERROR: Subject is longer than 32 symbols.\n";
            return 0;
        }

        $rows = array_map(function($v){return str_getcsv($v, ";");}, file($_SERVER['DOCUMENT_ROOT'].'log/log.csv'));
        $header = array_shift($rows);
        $csv    = [];
        foreach($rows as $row) {
            $csv[] = array_combine($header, $row);
        }

        $allUserNotifications = array();

        foreach ($rows as $key => $value) {
            if ($value[1] == $to->receiver()) {
                array_push($allUserNotifications, $value);
            }
        }

        $notificationsCount = count($allUserNotifications);

        $lastDate = $rows[$notificationsCount][0];

        $daysRemain =  date_diff(new \DateTime(), new \DateTime($lastDate))->days;

        if ($daysRemain == 0) {
            echo "ERROR: More than one notification per user per day.\n";
            return 0;
        }

        $this->sender->send($message, $to);
        $this->logger->log($message, $to);
    }
}
