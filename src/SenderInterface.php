<?php


namespace App;


interface SenderInterface
{
    public function send(Message $message, Address $to);
}