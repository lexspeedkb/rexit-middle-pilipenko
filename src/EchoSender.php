<?php
declare(strict_types=1);


namespace App;


final class EchoSender implements SenderInterface
{
    public function send(Message $message, Address $to)
    {
        echo sprintf("Notification for %s! Subject: %s\nMessage: %s\n", $to->receiver(), $message->subject(), $message->message());
    }
}
