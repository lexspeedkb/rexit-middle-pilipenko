<?php
declare(strict_types=1);


namespace App;


final class Logger implements LoggerInterface
{
    public function log(Message $message, Address $to)
    {
        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'log/log.csv', 'a');

        $fields = array (date('Y-m-d H:i:s'), $to->receiver(), $message->subject(), $message->message());

        fputcsv($fp, $fields, ';');


        fclose($fp);
    }
}
