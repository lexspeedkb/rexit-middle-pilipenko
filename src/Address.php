<?php
declare(strict_types=1);


namespace App;


final class Address
{
    private string $receiver;

    public function __construct(string $receiver)
    {
        $this->receiver = $receiver;
    }

    public function receiver(): string
    {
        return $this->receiver;
    }
}