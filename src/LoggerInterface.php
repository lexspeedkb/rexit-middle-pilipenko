<?php


namespace App;


interface LoggerInterface
{
    public function log(Message $message, Address $to);
}
