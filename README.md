PHP7.4

Для запуска команды достаточно выполнить ```php bin/app.php```

Вариант запуска с помощью докера

```docker container run --rm -v $(pwd):/app php:7.4-cli php /app/bin/app.php```

Удобнее всего запустить контейнер с пхп ```docker-compose up``` после чего внутри контейнера выполнять ```php bin/app.php```

Есть makefile. Использовать команды можно в формате ```make <command name>``` i.e. ```make start```

Комманды внутри makefile:
```
start                          clean env, run composer install, up docker-compose
stop                           stop environment
erase                          clean environment
build                          build environment and initialize composer and project dependencies
up                             spin up environment
bash                           run bash inside php container
phpunit                        execute project unit tests
help                           Display this help message
```

Гайд для использования makefile:

* ```make up``` (или для **первого запуска** ```make start```)
* ```make bash```
* ```php bin/app.php``` для запуска команды
* ```php ./vendor/bin/phpunit --configuration phpunit.xml``` для запуска тестов